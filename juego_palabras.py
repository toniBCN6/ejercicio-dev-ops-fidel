import random

nombres = ["Juan", "María", "Carlos", "Ana", "Luis", "Jose", "Pedro", "David", "Juan Jose", "Miguel", "Iñigo". "Ami, Abdon", "Anna", "Fidel"]
mascotas = ["Perro", "Gato", "Loro", "Hamster", "Tortuga", "PAto", "Cobaya", "Conejo", "Elefante", "Gato", "Gato", "Dragón, Aguila", "Nutria", "Gato"]

nombre_elegido = random.choice(nombres)
mascota_elegida = random.choice(mascotas)

mensaje = f"¡Hola! Tu nombre es {nombre_elegido} y tu mascota ideal es {mascota_elegida}."

print(mensaje)
